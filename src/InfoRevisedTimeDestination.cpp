// SPDX-FileCopyrightText: 2010 Brad Hards <bradh@frogmouth.net>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "InfoRevisedTimeDestination.h"

#include "Reader.h"

namespace QRtfReader
{
InfoRevisedTimeDestination::InfoRevisedTimeDestination(Reader *reader, AbstractRtfOutput *output, const QString &name)
    : InfoTimeDestination(reader, output, name)
{
}

InfoRevisedTimeDestination::~InfoRevisedTimeDestination()
{
}

void InfoRevisedTimeDestination::aboutToEndDestination()
{
    m_output->setRevisedDateTime(dateTime());
}
}
