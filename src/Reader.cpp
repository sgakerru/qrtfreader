// SPDX-FileCopyrightText: 2008, 2010 Brad Hards <bradh@frogmouth.net>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "Reader.h"
#include "AbstractRtfOutput.h"
#include "controlword.h"
#include "qrtfreader_debug.h"

#include "AuthorPcdataDestination.h"
#include "CategoryPcdataDestination.h"
#include "ColorTableDestination.h"
#include "CommentPcdataDestination.h"
#include "CompanyPcdataDestination.h"
#include "Destination.h"
#include "DocumentCommentPcdataDestination.h"
#include "DocumentDestination.h"
#include "FontTableDestination.h"
#include "GeneratorPcdataDestination.h"
#include "HLinkBasePcdataDestination.h"
#include "IgnoredDestination.h"
#include "InfoCreatedTimeDestination.h"
#include "InfoDestination.h"
#include "InfoPrintedTimeDestination.h"
#include "InfoRevisedTimeDestination.h"
#include "KeywordsPcdataDestination.h"
#include "ManagerPcdataDestination.h"
#include "OperatorPcdataDestination.h"
#include "PictDestination.h"
#include "RtfGroupState.h"
#include "StyleSheetDestination.h"
#include "SubjectPcdataDestination.h"
#include "TitlePcdataDestination.h"
#include "Tokenizer.h"
#include "UserPropsDestination.h"

#include <QStack>
#include <QTextCursor>
#include <QUrl>

namespace QRtfReader
{
Reader::Reader(QObject *parent)
    : QObject(parent)
    , m_inputDevice(nullptr)
    , m_output(nullptr)
{
}

Reader::~Reader()
{
    // Close file if open
    this->close();

    // Clean up any remaining objects
    qDeleteAll(m_destinationStack);
}

bool Reader::open(const QString &filename)
{
    m_inputDevice = new QFile(filename, this);

    bool result = m_inputDevice->open(QIODevice::ReadOnly);

    return result;
}

void Reader::close()
{
    if (!m_inputDevice) {
        return;
    }
    m_inputDevice->close();
    delete m_inputDevice;
    m_inputDevice = nullptr;
}

QString Reader::fileName() const
{
    if (m_inputDevice && m_inputDevice->exists()) {
        return m_inputDevice->fileName();
    } else {
        return QString();
    }
}

bool Reader::parseTo(AbstractRtfOutput *output)
{
    if ((!m_inputDevice) || (!m_inputDevice->isOpen())) {
        return false;
    }

    m_output = output;

    parseFile();

    return true;
}

void Reader::parseFile()
{
    Tokenizer tokenizer(m_inputDevice);

    if (parseFileHeader(tokenizer)) {
        parseDocument(tokenizer);
    }
}

bool Reader::parseFileHeader(Tokenizer &tokenizer)
{
    bool result = true;

    Token token = tokenizer.fetchToken();
    if (token.type != OpenGroup) {
        qCDebug(lcRtf) << "Not an RTF file";
        result = false;
    }

    token = tokenizer.fetchToken();
    if (token.type != Control) {
        qCDebug(lcRtf) << "Not an RTF file - wrong document type";
        result = false;
    }

    if (!headerFormatIsKnown(QString::fromUtf8(token.name), token.parameter.toInt())) {
        qCDebug(lcRtf) << "Not a valid RTF file - unknown header";
        result = false;
    }

    return result;
}

bool Reader::headerFormatIsKnown(const QString &tokenName, int tokenValue)
{
    if (tokenName != QLatin1String("rtf")) {
        qCDebug(lcRtf) << "unknown / unexpected header token name:" << tokenName;
        return false;
    }

    if (tokenValue != 1) {
        qCDebug(lcRtf) << "unknown / unexpected header token value:" << tokenValue;
        return false;
    }

    return true;
}

Destination *Reader::makeDestination(const QString &destinationName)
{
    if (destinationName == QLatin1String("colortbl")) {
        return new ColorTableDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("creatim")) {
        return new InfoCreatedTimeDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("printim")) {
        return new InfoPrintedTimeDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("revtim")) {
        return new InfoRevisedTimeDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("author")) {
        return new AuthorPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("company")) {
        return new CompanyPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("operator")) {
        return new OperatorPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("comment")) {
        return new CommentPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("doccomm")) {
        return new DocumentCommentPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("title")) {
        return new TitlePcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("subject")) {
        return new SubjectPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("manager")) {
        return new ManagerPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("category")) {
        return new CategoryPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("keywords")) {
        return new KeywordsPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("hlinkbase")) {
        return new HLinkBasePcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("generator")) {
        return new GeneratorPcdataDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("pict")) {
        return new PictDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("fonttbl")) {
        return new FontTableDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("stylesheet")) {
        return new StyleSheetDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("rtf")) {
        return new DocumentDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("info")) {
        return new InfoDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("userprops")) {
        return new UserPropsDestination(this, m_output, destinationName);
    } else if (destinationName == QLatin1String("ignorable")) {
        return new IgnoredDestination(this, m_output, destinationName);
    }
    qCDebug(lcRtf) << "creating plain old Destination for" << destinationName;
    return new Destination(this, m_output, destinationName);
}

void Reader::changeDestination(const QString &destinationName)
{
    if (m_destinationStack.top()->name() == QLatin1String("ignorable")) {
        // we don't change destinations inside ignored groups
        return;
    }
    // qCDebug(lcRtf) << m_debugIndent << "about to change destination to: " << destinationName;

    Destination *dest = makeDestination(destinationName);

    m_destinationStack.push(dest);
    m_stateStack.top().didChangeDestination = true;
    QStringList destStackElementNames;
    for (int i = 0; i < m_destinationStack.size(); ++i) {
        destStackElementNames << m_destinationStack.at(i)->name();
    }
    qCDebug(lcRtf) << m_debugIndent << "destinationStack after changeDestination (" << destStackElementNames << ")";
}

void Reader::parseDocument(Tokenizer &tokenizer)
{
    RtfGroupState state{};

    // Push an end-of-file marker onto the stack
    state.endOfFile = true;
    m_stateStack.push(state);

    // Set up the outer part of the destination stack
    Destination *dest = makeDestination(QStringLiteral("rtf"));
    m_destinationStack.push(dest);
    m_stateStack.top().didChangeDestination = true;

    m_debugIndent = QLatin1Char('\t');
    // Parse RTF document
    bool atEndOfFile = false;
    m_nextSymbolMightBeDestination = false;

    QRtfReader::ControlWord controlWord(QStringLiteral(""));

    while (!atEndOfFile) {
        Token token = tokenizer.fetchToken();
        // token.dump();
        switch (token.type) {
        case OpenGroup: {
            // Store the current state on the stack
            RtfGroupState state;
            m_stateStack.push(state);
            m_nextSymbolMightBeDestination = true;
            m_output->startGroup();
            // qCDebug(lcRtf) << m_debugIndent << "opengroup";
            m_debugIndent.append(QStringLiteral("\t"));
            break;
        }
        case CloseGroup: {
            QStringList destStackElementNames;
            for (int i = 0; i < m_destinationStack.size(); ++i) {
                destStackElementNames << m_destinationStack.at(i)->name();
            }
            // qCDebug(lcRtf) << m_debugIndent << "closegroup ( destinationStack:" << destStackElementNames << ")";
            m_debugIndent.remove(0, 1);
            state = m_stateStack.pop();
            if (state.endOfFile) {
                atEndOfFile = true;
            } else {
                m_output->endGroup();
            }

            if (state.didChangeDestination) {
                m_destinationStack.top()->aboutToEndDestination();
                delete m_destinationStack.top();
                m_destinationStack.pop();
            }

            destStackElementNames.clear();
            for (int i = 0; i < m_destinationStack.size(); ++i) {
                destStackElementNames << m_destinationStack.at(i)->name();
            }
            // qCDebug(lcRtf) << m_debugIndent << "destinationStack after CloseGroup: (" << destStackElementNames << ")";
            m_nextSymbolMightBeDestination = true;
            break;
        }
        case Control:
            controlWord = ControlWord(QString::fromUtf8(token.name));
            if (!controlWord.isKnown()) {
                qCDebug(lcRtf) << "*** Unrecognised control word (not in spec 1.9.1): " << token.name;
            }

            // LibreOffice put "*" token before this controlWord, so \ud destination
            // and inner destinations will be ignored (for example \title inside \ud).
            // But we want to keep inner content of this destination for metadata extraction.
            //
            // P.S Eventually unicode \ud inner data will rewrite latin encoding data
            // from \upr that can be in wrong encoding.
            if (token.name == "ud") {
                m_nextSymbolMightBeDestination = true;
                m_nextSymbolIsIgnorable = false;
            }
            // qCDebug(lcRtf) << m_debugIndent << "got controlWord: " << token.name;
            // qCDebug(lcRtf) << m_debugIndent << "isDestination:" << controlWord.isDestination();
            // qCDebug(lcRtf) << m_debugIndent << "isIgnorable:" << m_nextSymbolIsIgnorable;

            if (m_nextSymbolMightBeDestination && controlWord.isSupportedDestination()) {
                m_nextSymbolMightBeDestination = false;
                m_nextSymbolIsIgnorable = false;
                changeDestination(QString::fromUtf8(token.name));
            } else if (m_nextSymbolMightBeDestination && m_nextSymbolIsIgnorable) {
                // This is a control word we don't understand
                m_nextSymbolMightBeDestination = false;
                m_nextSymbolIsIgnorable = false;
                qCDebug(lcRtf) << "ignorable destination word:" << token.name;
                changeDestination(QStringLiteral("ignorable"));
            } else {
                m_nextSymbolMightBeDestination = false;
                if (token.name == "*") {
                    m_nextSymbolMightBeDestination = true;
                    m_nextSymbolIsIgnorable = true;
                }
                m_destinationStack.top()->handleControlWord(token.name, token.hasParameter, token.parameter.toInt());
            }
            break;
        case Plain:
            m_destinationStack.top()->handlePlainText(token.name);
            break;
        case Binary:
            qCDebug(lcRtf) << "binary data:" << token.name;
            break;
        default:
            qCDebug(lcRtf) << "Unexpected token Type";
        }
    }
}
}
